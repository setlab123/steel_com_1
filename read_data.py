#!/usr/bin/env python


# decoded = OrderedDict([
#     ('string', decoder.decode_string(8)),
#     ('bits', decoder.decode_bits()),
#     ('8int', decoder.decode_8bit_int()),
#     ('8uint', decoder.decode_8bit_uint()),
#     ('16int', decoder.decode_16bit_int()),
#     ('16uint', decoder.decode_16bit_uint()),
#     ('32int', decoder.decode_32bit_int()),
#     ('32uint', decoder.decode_32bit_uint()),
#     ('16float', decoder.decode_16bit_float()),
#     ('16float2', decoder.decode_16bit_float()),
#     ('32float', decoder.decode_32bit_float()),
#     ('32float2', decoder.decode_32bit_float()),
#     ('64int', decoder.decode_64bit_int()),
#     ('64uint', decoder.decode_64bit_uint()),
#     ('ignore', decoder.skip_bytes(8)),
#     ('64float', decoder.decode_64bit_float()),
#     ('64float2', decoder.decode_64bit_float()),
# ])

import sys
import time
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication, QMainWindow, QAction, qApp)
from PyQt5.QtGui import QPixmap, QColor, QIcon
from PyQt5.QtCore import QCoreApplication, QTimer, QSize

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient

import pickle
import struct
import main_wind
import copy
import socket

class Read_modbus(QMainWindow):
    def __init__(self):
        super().__init__()

        self.initUI()
        self.connect_modbus()

        self.init_socket()
        # транспортер выкидной
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_1 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_1 = 0

        # транспортер подающий
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_2 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_2 = 0


        # молотковая дробилка
        # --->>>
        # вкл/выкл
        self.hammer_on_off = 0
        # авария по перегреву
        self.hammer_crash_overload = 0
        # скорость вращения
        self.hammer_speed = 0
        # сработка на отключение транспортера
        self.hammer_conveyor_off = 0
        # <<<---
        # установка скорости мин (отключение транспортера)
        self.hammer_set_off_speed_min = 0
        # установка скорости мин (отключение транспортера)
        self.hammer_set_speed_on = 0

        # вал разрывной
        # --->>>
        # вращения прямое
        self.val_direction_1_p = 0
        self.val_direction_2_p = 0
        self.val_direction_3_p = 0
        self.val_direction_4_p = 0
        self.val_direction_5_p = 0
        self.val_direction_6_p = 0

        # вращения обратное
        self.val_direction_1_o = 0
        self.val_direction_2_o = 0
        self.val_direction_3_o = 0
        self.val_direction_4_o = 0
        self.val_direction_5_o = 0
        self.val_direction_6_o = 0

        # срабатывание токового реле
        self.val_current_relay_1 = 0
        self.val_current_relay_2 = 0
        self.val_current_relay_3 = 0
        self.val_current_relay_4 = 0
        self.val_current_relay_5 = 0
        self.val_current_relay_6 = 0

        # срабатывание по датчику скорости
        self.val_sensor_speed_1 = 0
        self.val_sensor_speed_2 = 0
        self.val_sensor_speed_3 = 0
        self.val_sensor_speed_4 = 0
        self.val_sensor_speed_5 = 0
        self.val_sensor_speed_6 = 0

        # перегрев электродвигателя
        self.overheating_motor_1 = 0
        self.overheating_motor_2 = 0
        self.overheating_motor_3 = 0
        self.overheating_motor_4 = 0
        self.overheating_motor_5 = 0
        self.overheating_motor_6 = 0

        # зажатие вкл (вала)
        self.val_clamping_on_1 = 0
        self.val_clamping_on_2 = 0
        self.val_clamping_on_3 = 0
        self.val_clamping_on_4 = 0
        self.val_clamping_on_5 = 0
        self.val_clamping_on_6 = 0

        # скорость вращения
        self.val_speed_1 = 0
        self.val_speed_2 = 0
        self.val_speed_3 = 0
        self.val_speed_4 = 0
        self.val_speed_5 = 0
        self.val_speed_6 = 0

        # количество поданных реверсов
        self.val_quantity_revers_1 = 0
        self.val_quantity_revers_2 = 0
        self.val_quantity_revers_3 = 0
        self.val_quantity_revers_4 = 0
        self.val_quantity_revers_5 = 0
        self.val_quantity_revers_6 = 0



        # <<<---
        # установка отключения по скорости min
        self.val_set_off_speed_min_1 = 0
        self.val_set_off_speed_min_2 = 0
        self.val_set_off_speed_min_3 = 0
        self.val_set_off_speed_min_4 = 0
        self.val_set_off_speed_min_5 = 0
        self.val_set_off_speed_min_6 = 0

        # установка времени включения
        self.val_set_on_time_1 = 0
        self.val_set_on_time_2 = 0
        self.val_set_on_time_3 = 0
        self.val_set_on_time_4 = 0
        self.val_set_on_time_5 = 0
        self.val_set_on_time_6 = 0

        # установка времени фильтрации(срабатывание)
        self.val_set_time_actuation_1 = 0
        self.val_set_time_actuation_2 = 0
        self.val_set_time_actuation_3 = 0
        self.val_set_time_actuation_4 = 0
        self.val_set_time_actuation_5 = 0
        self.val_set_time_actuation_6 = 0

        # установка времени на отсутствие импульсов
        self.val_set_time_pulse_off_1 = 0
        self.val_set_time_pulse_off_2 = 0
        self.val_set_time_pulse_off_3 = 0
        self.val_set_time_pulse_off_4 = 0
        self.val_set_time_pulse_off_5 = 0
        self.val_set_time_pulse_off_6 = 0


        # Настройки
        # <<<---
        # время вкл выкидного транспортера
        self.time_on_conveyor_1 = 0
        # время выкл выкидного транспортера
        self.time_off_conveyor_1 = 0
        # время выключения молотковой дробилки
        self.time_off_hammer = 0
        # предпусковой звонок
        self.pre_launch = 0
        # время прямого вращения
        self.time_direct_rotation = 0
        # время обратного вражения
        self.time_reverse_rotation = 0
        # пауза на остановку
        self.pause_to_stop = 0

    def initUI(self):
        # настроека таймера
        self.timer1 = QTimer(self)
        self.timer1.timeout.connect(self.read_modbus)
        self.timer1.start(1000)

        self.timer2 = QTimer(self)
        self.timer2.timeout.connect(self.send_socket)
        self.timer2.start(1000)

    def decode_2_8bit(self, data):

        # print("-"*60)
        # print(data.registers)
        # print()

        line_registers = "{0:b}".format(data.registers[0])

        array_reg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        index = len(array_reg)-1
        size_line = len(line_registers)
        index_line = size_line - 1

        while(index_line >= 0):
            array_reg[index] = int(line_registers[index_line])
            index = index - 1
            index_line = index_line - 1

        # print(array_reg)
        return array_reg


    def connect_modbus(self):
        self.client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
        connection = self.client.connect()
        print("Connection", connection)

    def read_modbus(self):
        s_time = time.time()
        # чтение 2х 8-bits  (reg №0)
        try:
            result = self.client.read_input_registers(0, 1,  unit=2)
            reg_0 = self.decode_2_8bit(result)
            reg_0 = copy.deepcopy(list(reversed(reg_0)))
            # print(reg_0)
        except AttributeError:
            print("ModbusIOException object has no attribute registers1")

        # чтение float (reg №2-3)
        try:
            result = self.client.read_holding_registers(2, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_2_3 = decoder.decode_32bit_float()
            # print(reg_2_3)
            self.hammer_speed = reg_2_3
        except AttributeError:
            print("ModbusIOException object has no attribute registers2")


        # чтение float (reg_4_5)
        try:
            result = self.client.read_holding_registers(4, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_4_5 = decoder.decode_32bit_float()
            # print(reg_4_5)
            self.hammer_set_off_speed_min = reg_4_5
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_6_7)
        try:
            result = self.client.read_holding_registers(6, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_6_7 = decoder.decode_32bit_float()
            # print(reg_6_7)
            self.hammer_set_speed_on = reg_4_5
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение 2х 8-bits  (reg_8)
        # вращение прямое; обратное
        try:
            result = self.client.read_input_registers(8, 1,  unit=2)
            reg_8 = self.decode_2_8bit(result)
            reg_8 = copy.deepcopy(list(reversed(reg_8)))
            # print(reg_8)
            # вращения прямое
            self.val_direction_1_p = reg_8[0]
            self.val_direction_2_p = reg_8[1]
            self.val_direction_3_p = reg_8[2]
            self.val_direction_4_p = reg_8[3]
            self.val_direction_5_p = reg_8[4]
            self.val_direction_6_p = reg_8[5]

            # вращения обратное
            self.val_direction_1_o = reg_8[8]
            self.val_direction_2_o = reg_8[9]
            self.val_direction_3_o = reg_8[10]
            self.val_direction_4_o = reg_8[11]
            self.val_direction_5_o = reg_8[12]
            self.val_direction_6_o = reg_8[13]
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение 2х 8-bits  (reg_9)
        # рабатывание токового реле, датчику скорости
        try:
            result = self.client.read_input_registers(9, 1,  unit=2)
            reg_9 = self.decode_2_8bit(result)
            reg_9 = copy.deepcopy(list(reversed(reg_9)))
            # print(reg_9)
            # срабатывание токового реле
            self.val_current_relay_1 = reg_9[0]
            self.val_current_relay_2 = reg_9[1]
            self.val_current_relay_3 = reg_9[2]
            self.val_current_relay_4 = reg_9[3]
            self.val_current_relay_5 = reg_9[4]
            self.val_current_relay_6 = reg_9[5]

            # срабатывание по датчику скорости
            self.val_sensor_speed_1 = reg_9[8]
            self.val_sensor_speed_2 = reg_9[9]
            self.val_sensor_speed_3 = reg_9[10]
            self.val_sensor_speed_4 = reg_9[11]
            self.val_sensor_speed_5 = reg_9[12]
            self.val_sensor_speed_6 = reg_9[13]
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # чтение 2х 8-bits  (reg_10)
        # перегрев электродвигателя; зажатие вала
        try:
            result = self.client.read_input_registers(10, 1,  unit=2)
            reg_10 = self.decode_2_8bit(result)
            reg_10 = copy.deepcopy(list(reversed(reg_10)))
            # print(reg_10)
            # перегрев электродвигателя
            self.overheating_motor_1 = reg_10[0]
            self.overheating_motor_2 = reg_10[1]
            self.overheating_motor_3 = reg_10[2]
            self.overheating_motor_4 = reg_10[3]
            self.overheating_motor_5 = reg_10[4]
            self.overheating_motor_6 = reg_10[5]

            # зажатие вкл (вала)
            self.val_clamping_on_1 = reg_10[8]
            self.val_clamping_on_2 = reg_10[9]
            self.val_clamping_on_3 = reg_10[10]
            self.val_clamping_on_4 = reg_10[11]
            self.val_clamping_on_5 = reg_10[12]
            self.val_clamping_on_6 = reg_10[13]
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # скорость вала
        # чтение float (reg_162_13)
        try:
            result = self.client.read_holding_registers(12, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_12_13 = decoder.decode_32bit_float()
            # print(reg_12_13)
            self.val_speed_1 = reg_12_13
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # чтение float (reg_14_15)
        try:
            result = self.client.read_holding_registers(14, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_14_15 = decoder.decode_32bit_float()
            # print(reg_14_15)
            self.val_speed_2 = reg_14_15
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # чтение float (reg_16_17)
        try:
            result = self.client.read_holding_registers(16, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_16_17 = decoder.decode_32bit_float()
            # print(reg_16_17)
            self.val_speed_3 = reg_16_17
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # чтение float (reg_18_19)
        try:
            result = self.client.read_holding_registers(18, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_18_19 = decoder.decode_32bit_float()
            # print(reg_18_19)
            self.val_speed_4 = reg_18_19
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_20_21)
        try:
            result = self.client.read_holding_registers(20, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_20_21 = decoder.decode_32bit_float()
            # print(reg_20_21)
            self.val_speed_5 = reg_20_21
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_22_23)
        try:
            result = self.client.read_holding_registers(22, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_22_23 = decoder.decode_32bit_float()
            # print(reg_22_23)
            self.val_speed_6 = reg_22_23
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # количество поданых реверсов
        # чтение float (reg_24_25)
        try:
            result = self.client.read_holding_registers(24, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_24_25 = decoder.decode_32bit_float()
            # print(reg_24_25)
            self.val_quantity_revers_1 = reg_24_25
        except AttributeError:
            print("ModbusIOException object has no attribute registers")
        # чтение float (reg_26_27)
        try:
            result = self.client.read_holding_registers(26, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_26_27 = decoder.decode_32bit_float()
            # print(reg_26_27)
            self.val_quantity_revers_2 = reg_26_27
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_28_29)
        try:
            result = self.client.read_holding_registers(28, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_28_29 = decoder.decode_32bit_float()
            # print(reg_28_29)
            self.val_quantity_revers_3 = reg_28_29
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_30_31)
        try:
            result = self.client.read_holding_registers(30, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_30_31 = decoder.decode_32bit_float()
            # print(reg_30_31)
            self.val_quantity_revers_4 = reg_30_31
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_32_33)
        try:
            result = self.client.read_holding_registers(32, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_32_33 = decoder.decode_32bit_float()
            # print(reg_32_33)
            self.val_quantity_revers_3 = reg_32_33
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_34_35)
        try:
            result = self.client.read_holding_registers(34, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_34_35 = decoder.decode_32bit_float()
            # print(reg_34_35)
            self.val_quantity_revers_4 = reg_34_35
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # установка отключения по скорости min
        # чтение float (reg_36_37)
        try:
            result = self.client.read_holding_registers(36, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_36_37 = decoder.decode_32bit_float()
            # print(reg_36_37)
            self.val_set_off_speed_min_1 = reg_36_37
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_38_39)
        try:
            result = self.client.read_holding_registers(38, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_38_39 = decoder.decode_32bit_float()
            # print(reg_38_39)
            self.val_set_off_speed_min_2 = reg_38_39
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_40_41)
        try:
            result = self.client.read_holding_registers(40, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_40_41 = decoder.decode_32bit_float()
            # print(reg_40_41)
            self.val_set_off_speed_min_3 = reg_40_41
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_42_43)
        try:
            result = self.client.read_holding_registers(42, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_42_43 = decoder.decode_32bit_float()
            # print(reg_42_43)
            self.val_set_off_speed_min_4 = reg_42_43
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_44_45)
        try:
            result = self.client.read_holding_registers(44, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_44_45 = decoder.decode_32bit_float()
            # print(reg_44_45)
            self.val_set_off_speed_min_5 = reg_44_45
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_46_47)
        try:
            result = self.client.read_holding_registers(46, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_46_47 = decoder.decode_32bit_float()
            # print(reg_46_47)
            self.val_set_off_speed_min_6 = reg_46_47
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # установка времени включенияя
        # чтение float (reg_48_49)
        try:
            result = self.client.read_holding_registers(48, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_48_49 = decoder.decode_32bit_float()
            # print(reg_48_49)
            self.val_set_on_time_1 = reg_48_49
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_50_51)
        try:
            result = self.client.read_holding_registers(50, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_50_51 = decoder.decode_32bit_float()
            # print(reg_50_51)
            self.val_set_on_time_2 = reg_50_51
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_52_53)
        try:
            result = self.client.read_holding_registers(52, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_52_53 = decoder.decode_32bit_float()
            # print(reg_52_53)
            self.val_set_on_time_3 = reg_52_53
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_54_55)
        try:
            result = self.client.read_holding_registers(54, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_54_55 = decoder.decode_32bit_float()
            # print(reg_54_55)
            self.val_set_on_time_4 = reg_54_55
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_56_57)
        try:
            result = self.client.read_holding_registers(56, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_56_57 = decoder.decode_32bit_float()
            # print(reg_56_57)
            self.val_set_on_time_5 = reg_56_57
        except AttributeError:
            print("ModbusIOException object has no attribute registers")
        # чтение float (reg_58_59)
        try:
            result = self.client.read_holding_registers(58, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_58_59 = decoder.decode_32bit_float()
            # print(reg_58_59)
            self.val_set_on_time_6 = reg_58_59
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # установка времени фильтрации (срабатывания)
        # чтение float (reg_60_61)
        try:
            result = self.client.read_holding_registers(60, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_60_61 = decoder.decode_32bit_float()
            # print(reg_60_61)
            self.val_set_time_actuation_1 = reg_60_61
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_62_63)
        try:
            result = self.client.read_holding_registers(62, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_62_63 = decoder.decode_32bit_float()
            # print(reg_62_63)
            self.val_set_time_actuation_2 = reg_62_63
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_64_65)
        try:
            result = self.client.read_holding_registers(64, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_64_65 = decoder.decode_32bit_float()
            # print(reg_64_65)
            self.val_set_time_actuation_3 = reg_64_65
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_66_67)
        try:
            result = self.client.read_holding_registers(66, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_66_67 = decoder.decode_32bit_float()
            # print(reg_66_67)
            self.val_set_time_actuation_4 = reg_66_67
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_68_69)
        try:
            result = self.client.read_holding_registers(68, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_68_69 = decoder.decode_32bit_float()
            # print(reg_68_69)
            self.val_set_time_actuation_5 = reg_68_69
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_70_71)
        try:
            result = self.client.read_holding_registers(70, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_70_71 = decoder.decode_32bit_float()
            # print(reg_70_71)
            self.val_set_time_actuation_6 = reg_70_71
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # установка времени на отсутствие импульсов
        # чтение float (reg_72_73)
        try:
            result = self.client.read_holding_registers(72, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_72_73 = decoder.decode_32bit_float()
            # print(reg_72_73)
            self.val_set_time_pulse_off_1 = reg_72_73
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_74_75)
        try:
            result = self.client.read_holding_registers(74, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_74_75 = decoder.decode_32bit_float()
            # print(reg_74_75)
            self.val_set_time_pulse_off_2 = reg_74_75
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_76_77)
        try:
            result = self.client.read_holding_registers(76, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_76_77 = decoder.decode_32bit_float()
            # print(reg_76_77)
            self.val_set_time_pulse_off_3 = reg_76_77
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_78_79)
        try:
            result = self.client.read_holding_registers(78, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_78_79 = decoder.decode_32bit_float()
            # print(reg_78_79)
            self.val_set_time_pulse_off_4 = reg_78_79
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_80_81)
        try:
            result = self.client.read_holding_registers(80, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_80_81 = decoder.decode_32bit_float()
            # print(reg_80_81)
            self.val_set_time_pulse_off_5 = reg_80_81
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # чтение float (reg_82_83)
        try:
            result = self.client.read_holding_registers(82, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_82_83 = decoder.decode_32bit_float()
            # print(reg_82_83)
            self.val_set_time_pulse_off_6 = reg_82_83
        except AttributeError:
            print("ModbusIOException object has no attribute registers")


        # настройки
        # время вкл выкидного транспортера
        # чтение float (reg_84_85)
        try:
            result = self.client.read_holding_registers(84, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_84_85 = decoder.decode_32bit_float()
            # print(reg_84_85)
            self.time_on_conveyor_1 = reg_84_85
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # время выкл выкидного транспортера
        # чтение float (reg_86_87)
        try:
            result = self.client.read_holding_registers(86, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_86_87 = decoder.decode_32bit_float()
            # print(reg_86_87)
            self.time_off_conveyor_1 = reg_86_87
        except AttributeError:
            print("ModbusIOException object has no attribute registers")
        # время выключения молотковой дробилки
        # чтение float (reg_88_89)
        try:
            result = self.client.read_holding_registers(88, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_88_89 = decoder.decode_32bit_float()
            # print(reg_88_89)
            self.time_off_hammer = reg_88_89
        except AttributeError:
            print("ModbusIOException object has no attribute registers")
        # предпусковой звонок
        # чтение float (reg_90_91)
        try:
            result = self.client.read_holding_registers(90, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_90_91 = decoder.decode_32bit_float()
            # print(reg_90_91)
            self.pre_launch = reg_90_91
        except AttributeError:
            print("ModbusIOException object has no attribute registers")

        # время прямого вращения
        # чтение float (reg_92_93)
        try:
            result = self.client.read_holding_registers(92, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_92_93 = decoder.decode_32bit_float()
            # print(reg_92_93)
            self.time_direct_rotation = reg_92_93
        except AttributeError:
            print("ModbusIOException object has no attribute registers")
        # время обратного вражения
        # чтение float (reg_94_95)
        try:
            result = self.client.read_holding_registers(94, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_94_95 = decoder.decode_32bit_float()
            # print(reg_94_95)
            self.time_reverse_rotation = reg_94_95
        except AttributeError:
            print("ModbusIOException object has no attribute registers")
        # пауза на остановку
        # чтение float (reg_96_97)
        try:
            result = self.client.read_holding_registers(96, 2,  unit=2)
            decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            reg_96_97 = decoder.decode_32bit_float()
            # print(reg_96_97)
            self.pause_to_stop = reg_96_97
        except AttributeError:
            print("ModbusIOException object has no attribute registers")
        f_time = time.time()
        d_time = f_time - s_time
        print(d_time)

    def init_socket(self):
        self.clientsocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.clientsocket.connect(('localhost',8089))

    def send_socket(self):
        print("send_socket")
        frame = [111,1,1,1,1,1,1.0,4444.66,4442]
        data = pickle.dumps(frame)
        message_size = struct.pack("L", len(data)) ###
        self.clientsocket.sendall(message_size + data)


if __name__ == '__main__':

    app = QApplication(sys.argv)
    read_obj = Read_modbus()
    read_obj.read_modbus()
    sys.exit(app.exec_())
