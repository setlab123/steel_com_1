#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication)
from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtCore import QCoreApplication, QTimer

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient
import copy

client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
connection = client.connect()
print(connection)

def decode_2_8bit(data):

    print("-"*60)
    print(data.registers)
    print()

    line_registers = "{0:b}".format(data.registers[0])

    array_reg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    index = len(array_reg)-1
    size_line = len(line_registers)
    index_line = size_line - 1

    while(index_line >= 0):
        array_reg[index] = int(line_registers[index_line])
        index = index - 1
        index_line = index_line - 1

    # print(array_reg)
    return array_reg

class Example(QWidget):

    # функция инициализации выполняется 1 раз
    def __init__(self):
        super().__init__()

        self.initUI()
        self.reg_3_4 = 0

        self.write_status = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        self.status =       [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        self.write_status_reg_3_4 = 0

        # чтение 2х 8-bits регистров
        # result = client.read_input_registers(0, 1,  unit=2)
        # array_reg = decode_2_8bit(result)
        # self.status = copy.deepcopy(list(reversed(array_reg)))

        # настроека таймера
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.readTime)
        self.timer.start(100)

    # функция реакции на таймер
    def readTime(self):

        # чтение 2х 8-bits  (reg №1)
        result = client.read_input_registers(9, 1,  unit=2)
        array_reg = decode_2_8bit(result)
        self.status = copy.deepcopy(list(reversed(array_reg)))
        print(self.status)

        # чтение float (reg №2-3)
        result = client.read_holding_registers(4, 2,  unit=2)
        decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
        reg_3_4 = decoder.decode_32bit_float()
        print(reg_3_4)

        self.qle_1.setText(str(reg_3_4))


        # запись побитовая в регистри
        if(self.write_status[0] == 1):
            client.write_coil(0, not self.status[0], unit=2)
            self.write_status[0] = 0

        if(self.write_status[1] == 1):
            client.write_coil(1, not self.status[1], unit=2)
            self.write_status[1] = 0

        if(self.write_status[2] == 1):
            client.write_coil(2, not self.status[2], unit=2)
            self.write_status[2] = 0


        # чтение float (reg №4-5)
        if(self.write_status_reg_3_4 == 1):
            builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
            builder.add_32bit_float(self.reg_3_4)
            registers = builder.to_registers()
            client.write_registers(2, registers, unit=2)

            self.write_status_reg_3_4 = 0


    def initUI(self):


        self.qbtn_1_m = QPushButton('0', self)
        self.qbtn_1_m.clicked.connect(self.click_0)
        self.qbtn_1_m.resize(25, 25)
        self.qbtn_1_m.move(50, 100)

        self.qbtn_2_m = QPushButton('1', self)
        self.qbtn_2_m.clicked.connect(self.click_1)
        self.qbtn_2_m.resize(25, 25)
        self.qbtn_2_m.move(100, 100)

        self.qbtn_3_m = QPushButton('2', self)
        self.qbtn_3_m.clicked.connect(self.click_2)
        self.qbtn_3_m.resize(25, 25)
        self.qbtn_3_m.move(150, 100)


        self.qbtn_load = QPushButton('Read data', self)
        self.qbtn_load.clicked.connect(self.click_read)
        self.qbtn_load.resize(100, 25)
        self.qbtn_load.move(300, 300)


        self.lbl_1 = QLabel(self)
        self.qle_1 = QLineEdit(self)

        self.lbl_1.move(50, 180)
        self.qle_1.move(50, 200)



        self.lbl_1.setText("From ПЛК")
        self.lbl_1.adjustSize()


        self.lbl_2 = QLabel(self)
        self.qle_2 = QLineEdit(self)

        self.lbl_2.move(250, 180)
        self.qle_2.move(250, 200)
        self.lbl_2.setText("To ПЛК")
        self.lbl_2.adjustSize()

        self.qle_2.textChanged[str].connect(self.onChanged_2)


        self.qbtn_load = QPushButton('To ПЛК', self)
        self.qbtn_load.clicked.connect(self.to_plk)
        self.qbtn_load.resize(100, 25)
        self.qbtn_load.move(250, 230)


        self.setGeometry(400, 400, 480, 360)
        self.setWindowTitle('Modbus OVEN')
        self.show()


    def click_0(self):
        self.write_status[0] = 1

    def click_1(self):
        self.write_status[1] = 1

    def click_2(self):
        self.write_status[2] = 1

    def to_plk(self):
        self.write_status_reg_3_4 = 1

    def click_read(self):
        print("read")


    def onChanged_2(self, text):
        try:
            self.reg_3_4 = float(text)
            self.qle_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")







if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
