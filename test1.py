#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication)
from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtCore import QCoreApplication

class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()
        self.kol = 0;
        self.dim = 0;

    def initUI(self):

        self.lbl_1 = QLabel(self)
        self.qle_1 = QLineEdit(self)

        self.lbl_1.move(20, 30)
        self.qle_1.move(20, 50)

        # self.qle_1.setReadOnly(True)


        self.lbl_1.setText("Количество, шт.")
        self.lbl_1.adjustSize()
        self.qle_1.textChanged[str].connect(self.onChanged_kol)



        self.lbl_2 = QLabel(self)
        self.qle_2 = QLineEdit(self)

        self.lbl_2.move(20, 90)
        self.qle_2.move(20, 110)

        self.lbl_2.setText("Размер, мм.")
        self.lbl_2.adjustSize()

        self.qle_2.textChanged[str].connect(self.onChanged_dim)



        self.qbtn_1_m = QPushButton('-', self)
        self.qbtn_1_m.clicked.connect(self.click_b_1_m)
        self.qbtn_1_m.resize(25, 25)
        self.qbtn_1_m.move(250, 50)

        self.qbtn_1_p = QPushButton('+', self)
        self.qbtn_1_p.clicked.connect(self.click_b_1_p)
        self.qbtn_1_p.resize(25, 25)
        self.qbtn_1_p.move(300, 50)

        self.qbtn_2_m = QPushButton('-', self)
        self.qbtn_2_m.clicked.connect(self.click_b_2_m)
        self.qbtn_2_m.resize(25, 25)
        self.qbtn_2_m.move(250, 110)

        self.qbtn_2_p = QPushButton('+', self)
        self.qbtn_2_p.clicked.connect(self.click_b_2_p)
        self.qbtn_2_p.resize(25, 25)
        self.qbtn_2_p.move(300, 110)


        self.qbtn_load = QPushButton('Отправить', self)
        self.qbtn_load.clicked.connect(self.click_load)
        self.qbtn_load.resize(100, 25)
        self.qbtn_load.move(300, 300)



        self.setGeometry(300, 300, 480, 360)
        self.setWindowTitle('Станок')
        self.show()


    def click_b_1_m(self, text):
        # print("-")
        if(self.kol > 0):
            self.kol = self.kol - 1
            self.qle_1.setText(str(self.kol))
    def click_b_1_p(self, text):
        # print("+")
        self.kol = self.kol + 1
        self.qle_1.setText(str(self.kol))

    def click_b_2_m(self, text):
        # print("-")
        if(self.dim > 0):
            self.dim = self.dim - 1
            self.qle_2.setText(str(self.dim))

    def click_b_2_p(self, text):
        # print("+")
        self.dim = self.dim + 1
        self.qle_2.setText(str(self.dim))

    def click_load(self, text):
        print("отправил")


    def onChanged_kol(self, text):
        try:
            self.kol = int(text)
            self.qle_1.setStyleSheet('background : #ffffff; ')
            print(self.kol)
        except ValueError:
            self.qle_1.setStyleSheet('background : #ff4000; ')
            print("ValueError")


    def onChanged_dim(self, text):
        try:
            self.dim = int(text)
            self.qle_2.setStyleSheet('background : #ffffff; ')
            print(self.dim)
        except ValueError:
            self.qle_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
