# coding=utf8
from pyModbusTCP.client import ModbusClient
import time
import sys

def main():
    ip_address='10.0.6.100'
    c = ModbusClient()
    if not c.host(ip_address):
        print("host error")
    if not c.port(502):
        print("port error")
    while True:
        if c.is_open():
            rr = c.read_discrete_inputs(0, 2)
            # [False, True]
            if not rr[0] and rr[1]:
                print('work')
                sys.exit(0)
            # [True, True]
            elif rr[0] and rr[1]:
                print('not work')
                sys.exit(1)
            print('-')
            sys.exit(-1)
        else:
            c.open()

if __name__ == "__main__":
    main()
