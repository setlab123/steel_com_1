#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication)
from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtCore import QCoreApplication

from pymodbus.client.sync import ModbusSerialClient
import copy

client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
connection = client.connect()
print(connection)


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()
        self.kol = 0;
        self.dim = 0;
        self.status = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

        array_reg = self.read_status()

        self.status = copy.deepcopy(array_reg)
        
        self.status_dis = copy.deepcopy(self.status)

        print(self.status)
        for i in range(len(self.status)):
            self.status[i] = int(not self.status[i])
        print(self.status)
        # self.status_0 = array_reg[0]
        # self.status_1 = array_reg[1]
        # self.status_2 = array_reg[2]
        # self.status_3 = array_reg[3]
        # self.status_4 = array_reg[4]
        # self.status_5 = array_reg[5]
        # self.status_6 = array_reg[6]
        # self.status_7 = array_reg[7]

        # self.status_0 = 0
        # self.status_1 = 0
        # self.status_2 = 0
        # self.status_3 = 0
        # self.status_4 = 0
        # self.status_5 = 0
        # self.status_6 = 0
        # self.status_7 = 0


    def read_status(self):
        data = client.read_input_registers(0, 1, unit=2)

        print(data)
        print(data.registers)

        line_registers = "{0:b}".format(data.registers[0])
        print(line_registers)

        array_reg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        print(len(line_registers))

        index = len(array_reg)-1
        size_line = len(line_registers)
        index_line = size_line - 1

        while(index_line >= -1):
            print(array_reg)
            array_reg[index] = int(line_registers[index_line])
            index = index - 1
            index_line = index_line - 1

        # self.status_0 = array_reg[0]
        # self.status_1 = array_reg[1]
        # self.status_2 = array_reg[2]
        # self.status_3 = array_reg[3]
        # self.status_4 = array_reg[4]
        # self.status_5 = array_reg[5]
        # self.status_6 = array_reg[6]
        # self.status_7 = array_reg[7]
        return array_reg

    def initUI(self):




        self.qbtn_1_m = QPushButton('0', self)
        self.qbtn_1_m.clicked.connect(self.click_0)
        self.qbtn_1_m.resize(25, 25)
        self.qbtn_1_m.move(50, 100)

        self.qbtn_2_m = QPushButton('1', self)
        self.qbtn_2_m.clicked.connect(self.click_1)
        self.qbtn_2_m.resize(25, 25)
        self.qbtn_2_m.move(100, 100)

        self.qbtn_3_m = QPushButton('2', self)
        self.qbtn_3_m.clicked.connect(self.click_2)
        self.qbtn_3_m.resize(25, 25)
        self.qbtn_3_m.move(150, 100)

        self.qbtn_4_m = QPushButton('3', self)
        self.qbtn_4_m.clicked.connect(self.click_3)
        self.qbtn_4_m.resize(25, 25)
        self.qbtn_4_m.move(200, 100)

        self.qbtn_5_m = QPushButton('4', self)
        self.qbtn_5_m.clicked.connect(self.click_4)
        self.qbtn_5_m.resize(25, 25)
        self.qbtn_5_m.move(250, 100)

        self.qbtn_6_m = QPushButton('5', self)
        self.qbtn_6_m.clicked.connect(self.click_5)
        self.qbtn_6_m.resize(25, 25)
        self.qbtn_6_m.move(300, 100)

        self.qbtn_7_m = QPushButton('6', self)
        self.qbtn_7_m.clicked.connect(self.click_6)
        self.qbtn_7_m.resize(25, 25)
        self.qbtn_7_m.move(350, 100)

        self.qbtn_8_m = QPushButton('7', self)
        self.qbtn_8_m.clicked.connect(self.click_7)
        self.qbtn_8_m.resize(25, 25)
        self.qbtn_8_m.move(400, 100)



        self.qbtn_load = QPushButton('Read data', self)
        self.qbtn_load.clicked.connect(self.click_read)
        self.qbtn_load.resize(100, 25)
        self.qbtn_load.move(300, 300)



        # text_1 = str(self.status_dis[0])
        # self.lbl_1 = QLabel(self)
        # self.lbl_1.setText(text_1)
        # self.lbl_1.move(50, 70)

        self.setGeometry(300, 300, 480, 360)
        self.setWindowTitle('Modbus OVEN')
        self.show()
        # self.showFullScreen()

    def click_0(self):
        client.write_coil(0, self.status[0], unit=2)
        self.status[0] = not self.status[0]

    def click_1(self):
        client.write_coil(1, self.status[1], unit=2)
        self.status[1] = not self.status[1]

    def click_2(self):
        client.write_coil(2, self.status[2], unit=2)
        self.status[2] = not self.status[2]

    def click_3(self):
        client.write_coil(3, self.status[3], unit=2)
        self.status[3] = not self.status[3]

    def click_4(self):
        client.write_coil(4, self.status[4], unit=2)
        self.status[4] = not self.status[4]

    def click_5(self):
        client.write_coil(5, self.status[5], unit=2)
        self.status[5] = not self.status[5]

    def click_6(self):
        client.write_coil(6, self.status[6], unit=2)
        self.status[6] = not self.status[6]

    def click_7(self):
        client.write_coil(7, self.status[7], unit=2)
        self.status[7] = not self.status[7]



    def click_read(self):
        print("read")




if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
