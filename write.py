#!/usr/bin/env python


# builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
# builder.add_string('abcdefgh')
# builder.add_bits([0, 1, 0, 1, 1, 0, 1, 0])
# builder.add_8bit_int(-0x12)
# builder.add_8bit_uint(0x12)
# builder.add_16bit_int(-0x5678)
# builder.add_16bit_uint(0x1234)
# builder.add_32bit_int(-0x1234)
# builder.add_32bit_uint(0x12345678)
# builder.add_16bit_float(12.34)
# builder.add_16bit_float(-12.34)
# builder.add_32bit_float(22.34)
# builder.add_32bit_float(-22.34)
# builder.add_64bit_int(-0xDEADBEEF)
# builder.add_64bit_uint(0x12345678DEADBEEF)
# builder.add_64bit_uint(0x12345678DEADBEEF)
# builder.add_64bit_float(123.45)
# builder.add_64bit_float(-123.45)
# payload = builder.to_registers()



from pymodbus.constants import Endian
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder

import struct

client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
connection = client.connect()
print(connection)




builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)
builder.add_32bit_float(22.34)

registers = builder.to_registers()
client.write_registers(2, registers, unit=2)


# data = struct.pack('f', 2)
# print(data)
# client.write_registers(2, 33, unit=2)

client.close()
