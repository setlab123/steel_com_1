#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QLabel,QPushButton, QApplication,
    QLineEdit, QApplication, QMainWindow, QAction, qApp)
from PyQt5.QtGui import QPixmap, QColor, QIcon
from PyQt5.QtCore import QCoreApplication, QTimer, QSize


# --->>> на дисплей
# <<<--- на контроллер

class Window2(QMainWindow):                           # <===
    def __init__(self):
        super().__init__()

        self.qbtn_load = QPushButton('Установить настройки', self)
        self.qbtn_load.clicked.connect(self.click_read)
        # self.qbtn_load.resize(100, 25)
        self.qbtn_load.adjustSize()
        self.qbtn_load.move(50, 400)


        self.lbl_to = QLabel(self)
        self.lbl_to.setText("Задать настройки на ПЛК")
        self.lbl_to.adjustSize()
        self.lbl_to.move(50, 10)

        self.lbl_to_1 = QLabel(self)
        self.lbl_to_1.setText("Время ??? ???? транспортера")
        self.lbl_to_1.adjustSize()
        self.lbl_to_1.move(50, 50)

        self.qle_to_1 = QLineEdit(self)
        self.qle_to_1.resize(200, 25)
        self.qle_to_1.move(50, 70)

        self.lbl_to_2 = QLabel(self)
        self.lbl_to_2.setText("Время ??? ???? транспортера")
        self.lbl_to_2.adjustSize()
        self.lbl_to_2.move(50, 100)

        self.qle_to_2 = QLineEdit(self)
        self.qle_to_2.resize(200, 25)
        self.qle_to_2.move(50, 120)

        self.lbl_to_3 = QLabel(self)
        self.lbl_to_3.setText("Время ??? молотковой дробилки")
        self.lbl_to_3.adjustSize()
        self.lbl_to_3.move(50, 150)

        self.qle_to_3 = QLineEdit(self)
        self.qle_to_3.resize(200, 25)
        self.qle_to_3.move(50, 170)

        self.lbl_to_4 = QLabel(self)
        self.lbl_to_4.setText("Предпусковой?? звонок (вкл/выкл)")
        self.lbl_to_4.adjustSize()
        self.lbl_to_4.move(50, 200)

        self.qle_to_4 = QLineEdit(self)
        self.qle_to_4.resize(200, 25)
        self.qle_to_4.move(50, 220)

        self.lbl_to_5 = QLabel(self)
        self.lbl_to_5.setText("Время прямого вращения")
        self.lbl_to_5.adjustSize()
        self.lbl_to_5.move(50, 250)

        self.qle_to_5 = QLineEdit(self)
        self.qle_to_5.resize(200, 25)
        self.qle_to_5.move(50, 270)

        self.lbl_to_6 = QLabel(self)
        self.lbl_to_6.setText("Время обратного вращения")
        self.lbl_to_6.adjustSize()
        self.lbl_to_6.move(50, 300)

        self.qle_to_6 = QLineEdit(self)
        self.qle_to_6.resize(200, 25)
        self.qle_to_6.move(50, 320)

        self.lbl_to_7 = QLabel(self)
        self.lbl_to_7.setText("????? на остановку")
        self.lbl_to_7.adjustSize()
        self.lbl_to_7.move(50, 350)

        self.qle_to_7 = QLineEdit(self)
        self.qle_to_7.resize(200, 25)
        self.qle_to_7.move(50, 370)


        self.lbl_from = QLabel(self)
        self.lbl_from.setText("Текущие настройки на ПЛК")
        self.lbl_from.adjustSize()
        self.lbl_from.move(300, 10)

        self.lbl_from_1 = QLabel(self)
        self.lbl_from_1.setText("Время ??? ???? транспортера")
        self.lbl_from_1.adjustSize()
        self.lbl_from_1.move(300, 50)

        self.qle_from_1 = QLineEdit(self)
        self.qle_from_1.resize(200, 25)
        self.qle_from_1.move(300, 70)

        self.lbl_from_2 = QLabel(self)
        self.lbl_from_2.setText("Время ??? ???? транспортера")
        self.lbl_from_2.adjustSize()
        self.lbl_from_2.move(300, 100)

        self.qle_from_2 = QLineEdit(self)
        self.qle_from_2.resize(200, 25)
        self.qle_from_2.move(300, 120)

        self.lbl_from_3 = QLabel(self)
        self.lbl_from_3.setText("Время ??? молотковой дробилки")
        self.lbl_from_3.adjustSize()
        self.lbl_from_3.move(300, 150)

        self.qle_from_3 = QLineEdit(self)
        self.qle_from_3.resize(200, 25)
        self.qle_from_3.move(300, 170)

        self.lbl_from_4 = QLabel(self)
        self.lbl_from_4.setText("Предпусковой?? звонок (вкл/выкл)")
        self.lbl_from_4.adjustSize()
        self.lbl_from_4.move(300, 200)

        self.qle_from_4 = QLineEdit(self)
        self.qle_from_4.resize(200, 25)
        self.qle_from_4.move(300, 220)

        self.lbl_from_5 = QLabel(self)
        self.lbl_from_5.setText("Время прямого вращения")
        self.lbl_from_5.adjustSize()
        self.lbl_from_5.move(300, 250)

        self.qle_from_5 = QLineEdit(self)
        self.qle_from_5.resize(200, 25)
        self.qle_from_5.move(300, 270)

        self.lbl_from_6 = QLabel(self)
        self.lbl_from_6.setText("Время обратного вращения")
        self.lbl_from_6.adjustSize()
        self.lbl_from_6.move(300, 300)

        self.qle_from_6 = QLineEdit(self)
        self.qle_from_6.resize(200, 25)
        self.qle_from_6.move(300, 320)

        self.lbl_from_7 = QLabel(self)
        self.lbl_from_7.setText("????? на остановку")
        self.lbl_from_7.adjustSize()
        self.lbl_from_7.move(300, 350)

        self.qle_from_7 = QLineEdit(self)
        self.qle_from_7.resize(200, 25)
        self.qle_from_7.move(300, 370)



        self.setGeometry(100, 100, 600, 600)
        self.setWindowTitle("Настройки")



    def to_plk(self):
        self.write_status_reg_3_4 = 1

    def click_read(self):
        print("read")


    def onChanged_2(self, text):
        try:
            self.reg_3_4 = float(text)
            self.qle_2.setStyleSheet('background : #ffffff; ')
        except ValueError:
            self.qle_2.setStyleSheet('background : #ff4000; ')
            print("ValueError")

class Window_Ui(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()

        # транспортер выкидной
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_1 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_1 = 0

        # транспортер ???????
        # --->>>
        # вкл/выкл
        self.conveyor_on_off_2 = 0
        # аварийное отключение по токовому реле
        self.conveyor_current_relay_2 = 0


        # молотковая дробилка
        # --->>>
        # вкл/выкл
        self.hammer_on_off = 0
        # авария по перегреву
        self.hammer_crash_overload = 0
        # скорость вращения
        self.hammer_speed = 0
        # сработка на отключение транспортера
        self.hammer_conveyor_off = 0
        # <<<---
        # установка скорости мин (отключение транспортера)
        self.hammer_set_off_speed_min = 0
        # установка скорости мин (отключение транспортера)
        self.hammer_set_speed_on = 0

        # вал разрывной
        # --->>>
        # вращения прямое
        self.val_direction_1_p = 0
        self.val_direction_2_p = 0
        self.val_direction_3_p = 0
        self.val_direction_4_p = 0
        self.val_direction_5_p = 0
        self.val_direction_6_p = 0

        # вращения обратное
        self.val_direction_1_o = 0
        self.val_direction_2_o = 0
        self.val_direction_3_o = 0
        self.val_direction_4_o = 0
        self.val_direction_5_o = 0
        self.val_direction_6_o = 0

        # срабатывание токового реле
        self.val_current_relay_1 = 0
        self.val_current_relay_2 = 0
        self.val_current_relay_3 = 0
        self.val_current_relay_4 = 0
        self.val_current_relay_5 = 0
        self.val_current_relay_6 = 0

        # срабатывание по датчику скорости
        self.val_sensor_speed_1 = 0
        self.val_sensor_speed_2 = 0
        self.val_sensor_speed_3 = 0
        self.val_sensor_speed_4 = 0
        self.val_sensor_speed_5 = 0
        self.val_sensor_speed_6 = 0

        # скорость вращения
        self.val_speed_1 = 0
        self.val_speed_2 = 0
        self.val_speed_3 = 0
        self.val_speed_4 = 0
        self.val_speed_5 = 0
        self.val_speed_6 = 0

        # перегрев электродвигателя
        self.overheating_motor_1 = 0
        self.overheating_motor_2 = 0
        self.overheating_motor_3 = 0
        self.overheating_motor_4 = 0
        self.overheating_motor_5 = 0
        self.overheating_motor_6 = 0

        # <<<---
        # установка отключения по скорости min
        self.val_set_off_speed_min_1 = 0
        self.val_set_off_speed_min_2 = 0
        self.val_set_off_speed_min_3 = 0
        self.val_set_off_speed_min_4 = 0
        self.val_set_off_speed_min_5 = 0
        self.val_set_off_speed_min_6 = 0

        # установка времени включения
        self.val_set_on_time_1 = 0
        self.val_set_on_time_2 = 0
        self.val_set_on_time_3 = 0
        self.val_set_on_time_4 = 0
        self.val_set_on_time_5 = 0
        self.val_set_on_time_6 = 0

        # установка времени фильтрации(срабатывание)


        # установка времени на отсутствие импульсов
        self.val_set_time_pulse_off_1 = 0
        self.val_set_time_pulse_off_2 = 0
        self.val_set_time_pulse_off_3 = 0
        self.val_set_time_pulse_off_4 = 0
        self.val_set_time_pulse_off_5 = 0
        self.val_set_time_pulse_off_6 = 0

        # количество поданных реверсов

        # ?






        # переменные для анимации

        self.alfa1 = 0
        self.alfa2 = 0
        self.alfa3 = 0
        self.alfa4 = 0
        self.alfa5 = 0
        self.alfa6 = 0

        self.alfa_hammer = 0

        self.alfa_conveyor_val_1 = 0
        self.alfa_conveyor_val_2 = 0

        self.test_x = 50

    def initUI(self):


        self.drob = QLabel(self)
        self.drob.setPixmap(QPixmap('дробилка4.png'))
        self.drob.setGeometry(0, 0, 1280, 800)

        self.tooth1 = QLabel(self)
        self.tooth1.setPixmap(QPixmap('зуб2/0.png'))
        self.tooth1.setGeometry(137, 326, 50, 50)

        self.tooth2 = QLabel(self)
        self.tooth2.setPixmap(QPixmap('зуб/0.png'))
        self.tooth2.setGeometry(193, 326, 50, 50)

        self.tooth3 = QLabel(self)
        self.tooth3.setPixmap(QPixmap('зуб2/0.png'))
        self.tooth3.setGeometry(250, 326, 50, 50)

        self.tooth4 = QLabel(self)
        self.tooth4.setPixmap(QPixmap('зуб/0.png'))
        self.tooth4.setGeometry(308, 326, 50, 50)

        self.tooth5 = QLabel(self)
        self.tooth5.setPixmap(QPixmap('зуб2/0.png'))
        self.tooth5.setGeometry(366, 326, 50, 50)

        self.tooth6 = QLabel(self)
        self.tooth6.setPixmap(QPixmap('зуб/0.png'))
        self.tooth6.setGeometry(422, 326, 50, 50)


        self.hammer = QLabel(self)
        self.hammer.setPixmap(QPixmap('молотковое_колесо/0.png'))
        self.hammer.setGeometry(733, 557, 80, 80)

        self.conveyor_val_1 = QLabel(self)
        self.conveyor_val_1.setPixmap(QPixmap('вал_транспортера/0.png'))
        self.conveyor_val_1.setGeometry(35, 565, 80, 80)

        self.conveyor_val_2 = QLabel(self)
        self.conveyor_val_2.setPixmap(QPixmap('вал_транспортера/0.png'))
        self.conveyor_val_2.setGeometry(1210, 595, 80, 80)


        self.qbtn_1_m = QPushButton('', self)
        self.qbtn_1_m.clicked.connect(self.window_setting)
        self.qbtn_1_m.setIcon(QIcon('setting.png'))
        self.qbtn_1_m.setIconSize(QSize(45, 45))
        self.qbtn_1_m.resize(50, 50)
        self.qbtn_1_m.move(0, 0)


        self.setGeometry(100, 100, 1280, 800)
        self.setWindowTitle('Modbus')
        self.show()

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.animation)
        self.timer.start(100)

    def animation_tooth(self, p, o, tooth, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'зуб2/' + str(alfa) + '.png'
        tooth.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_tooth2(self, p, o, tooth, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'зуб/' + str(alfa) + '.png'
        tooth.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_hammer(self, p, o, hammer, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'молотковое_колесо/' + str(alfa) + '.png'
        hammer.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation_conveyor_val(self, p, o, hammer, alfa):

        if(p and not o):
            if(alfa >= 350):
                alfa = 0
            alfa = alfa + 10
        elif(not p and o):
            if(alfa <= 0):
                alfa = 350
            alfa = alfa - 10
        elif(not p and not o):
            alfa = alfa

        name2 = 'вал_транспортера/' + str(alfa) + '.png'
        hammer.setPixmap(QPixmap(name2))
        # возвращаем alfa для обновления переменной после выполнения функци
        return alfa

    def animation(self):

        self.test_x = self.test_x - 1

        if(self.test_x>0):
            self.val_direction_1_p = 1
            self.val_direction_1_o = 0
        elif(self.test_x==0):
            self.val_direction_1_p = 0
            self.val_direction_1_o = 0
        elif(self.test_x<0):
            self.val_direction_1_p = 0
            self.val_direction_1_o = 1

        print(self.val_direction_1_p, self.val_direction_1_o)

        # тестовая проверка вращения зубьев
        self.alfa1 = self.animation_tooth(self.val_direction_1_p, self.val_direction_1_o, self.tooth1, self.alfa1)
        self.alfa2 = self.animation_tooth2(self.val_direction_1_p, self.val_direction_1_o, self.tooth2, self.alfa2)
        self.alfa3 = self.animation_tooth(self.val_direction_1_p, self.val_direction_1_o, self.tooth3, self.alfa3)
        self.alfa4 = self.animation_tooth2(self.val_direction_1_p, self.val_direction_1_o, self.tooth4, self.alfa4)
        self.alfa5 = self.animation_tooth(self.val_direction_1_p, self.val_direction_1_o, self.tooth5, self.alfa5)
        self.alfa6 = self.animation_tooth2(self.val_direction_1_p, self.val_direction_1_o, self.tooth6, self.alfa6)

        self.alfa_hammer = self.animation_hammer(0, 1, self.hammer, self.alfa_hammer)

        self.alfa_conveyor_val_1 = self.animation_conveyor_val(1, 0, self.conveyor_val_1, self.alfa_conveyor_val_1)
        self.alfa_conveyor_val_2 = self.animation_conveyor_val(1, 0, self.conveyor_val_2, self.alfa_conveyor_val_2)


    def window_setting(self):
        self.setting = Window2()
        self.setting.show()
        # self.hide()

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Window_Ui()
    # menus = MenuDemo()
    # ex = Main_Window()
    sys.exit(app.exec_())
