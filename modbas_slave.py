#!/usr/bin/env python

from pymodbus.client.sync import ModbusSerialClient

client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
connection = client.connect()
print(connection)

client.write_coil(0, 1)
# data = client.read_input_registers(0, 1, unit=1)
# print(data)
# print(data.registers)

# def read():
#     data = client.read_input_registers(0, 1, unit=2)
#
#     print(data)
#     print(data.registers)
#
#     line_registers = "{0:b}".format(data.registers[0])
#     print(line_registers)
#
#     array_reg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
#     print(len(line_registers))
#
#     index = len(array_reg)-1
#     size_line = len(line_registers)
#     index_line = size_line - 1
#
#     while(index_line >= -1):
#         print(array_reg)
#         array_reg[index] = int(line_registers[index_line])
#         index = index - 1
#         index_line = index_line - 1
#
#     return array_reg
#
# read()

client.close()
