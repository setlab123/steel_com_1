import serial
import time
from pymodbus.client.sync import ModbusSerialClient as ModbusClient

ser = serial.Serial('/dev/ttyUSB0',115200)
time.sleep(2)

# The delay and port setup above required to allow Arduino to reset
# before client setup below.

client =  ModbusClient(method='rtu', port='/dev/ttyUSB0', baudrate=115200, bytesize=8, stopbits=1, timeout=0.0)
connection = client.connect()
print(connection)
x = 0

while(1):

    x = x +1

    print ("Read input registers at Add 1")
    values = client.read_input_registers(address = 1,count =2,unit=1)
    print(values)
    print (values.registers)
    print ("")
    #
    # if(x>0):
    #     client.write_registers(address, registers, unit=1)

# print ("Read input registers at Add 1")
# values = client.read_input_registers(address = 1,count =6,unit=1)
# print (values.registers)
# print ("")
#
# print ("Read discrete inputs at Add 0")
# values = client.read_discrete_inputs(address = 1,count =7,unit=1)
# print (values)
# print ("")
#
# print ("Read holding registers at Add 0")
# values = client.read_holding_registers(address = 1,count =5,unit=1)
# print (values)
# print ("")
#
# print ("Read coils at Add 0")
# values = client.read_coils(1,3, unit=1)
# print (values)
# print ("")

client.close()
