#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QSlider, QFrame,
    QLabel, QApplication, QLineEdit)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QColor


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):
        self.col = QColor(0, 0, 0)


        sldR = QSlider(Qt.Horizontal, self)
        sldR.setFocusPolicy(Qt.NoFocus)
        sldR.setGeometry(100, 100, 255, 30)
        sldR.setMinimum(0)
        sldR.setMaximum(255)
        sldR.valueChanged[int].connect(self.changeValue_R)

        sldG = QSlider(Qt.Horizontal, self)
        sldG.setFocusPolicy(Qt.NoFocus)
        sldG.setGeometry(100, 150, 255, 30)
        sldG.setMinimum(0)
        sldG.setMaximum(255)
        sldG.valueChanged[int].connect(self.changeValue_G)

        sldB = QSlider(Qt.Horizontal, self)
        sldB.setFocusPolicy(Qt.NoFocus)
        sldB.setGeometry(100, 200, 255, 30)
        sldB.setMinimum(0)
        sldB.setMaximum(255)
        sldB.valueChanged[int].connect(self.changeValue_B)

        self.square = QFrame(self)
        self.square.setGeometry(400,100, 100, 100)
        self.square.setStyleSheet("QWidget { background-color: %s }" %
            self.col.name())



        self.label = QLabel(self)


        self.setGeometry(300, 300, 720, 480)
        self.setWindowTitle('LED control')
        self.show()


    def changeValue_R(self, value):
        print("r",value)
        self.col.setRed(value)
        self.square.setStyleSheet("QFrame { background-color: %s }" %
            self.col.name())

    def changeValue_G(self, value):
        print("g",value)
        self.col.setGreen(value)
        self.square.setStyleSheet("QFrame { background-color: %s }" %
            self.col.name())

    def changeValue_B(self, value):
        print("b",value)
        self.col.setBlue(value)
        self.square.setStyleSheet("QFrame { background-color: %s }" %
            self.col.name())

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
