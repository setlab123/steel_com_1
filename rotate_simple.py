# USAGE
# python rotate_simple.py --image images/saratoga.jpg

# import the necessary packages
import numpy as np
import argparse
import imutils
import cv2



# load the image from disk
image = cv2.imread("val/0.png")

print(image)

# loop over the rotation angles again, this time ensuring
# no part of the image is cut off
i = 0
for angle in np.arange(0, 360, 10):
	i = i + 1
	rotated = imutils.rotate_bound(image, angle)
	cv2.imwrite("val/"+str(i)+".png",rotated)
	# cv2.imshow("Rotated (Correct)", rotated)
	cv2.waitKey(27)
