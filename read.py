#!/usr/bin/env python


# decoded = OrderedDict([
#     ('string', decoder.decode_string(8)),
#     ('bits', decoder.decode_bits()),
#     ('8int', decoder.decode_8bit_int()),
#     ('8uint', decoder.decode_8bit_uint()),
#     ('16int', decoder.decode_16bit_int()),
#     ('16uint', decoder.decode_16bit_uint()),
#     ('32int', decoder.decode_32bit_int()),
#     ('32uint', decoder.decode_32bit_uint()),
#     ('16float', decoder.decode_16bit_float()),
#     ('16float2', decoder.decode_16bit_float()),
#     ('32float', decoder.decode_32bit_float()),
#     ('32float2', decoder.decode_32bit_float()),
#     ('64int', decoder.decode_64bit_int()),
#     ('64uint', decoder.decode_64bit_uint()),
#     ('ignore', decoder.skip_bytes(8)),
#     ('64float', decoder.decode_64bit_float()),
#     ('64float2', decoder.decode_64bit_float()),
# ])

from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.client.sync import ModbusSerialClient

client = ModbusSerialClient(method="rtu", port="COM15", stopbits=1, bytesize=8, parity="N", baudrate=115200, timeout=0.2)
connection = client.connect()
print(connection)

result = client.read_input_registers(9, 1,  unit=2)
print(result.registers)
decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
print(decoder)

out = decoder.decode_bits()
print(out)

result = client.read_holding_registers(2, 2,  unit=2)
decoder = BinaryPayloadDecoder.fromRegisters(result.registers, byteorder=Endian.Big, wordorder=Endian.Little)
print(decoder)

out = decoder.decode_32bit_float()
print(out)


# data = client.read_input_registers(2, 2, unit=2)
# print(data)
# print(data.registers)



def read():
    data = client.read_input_registers(0, 1, unit=2)

    print(data)
    print(data.registers)

    line_registers = "{0:b}".format(data.registers[0])
    # print(line_registers)

    array_reg = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    # print(len(line_registers))

    index = len(array_reg)-1
    size_line = len(line_registers)
    index_line = size_line - 1

    while(index_line >= -1):
        # print(array_reg)
        array_reg[index] = int(line_registers[index_line])
        index = index - 1
        index_line = index_line - 1

    print(array_reg)
    return array_reg
# read()

client.close()
